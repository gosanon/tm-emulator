function assertNotUndefined ({ ...vars }) {
  const undefinedVars = [];

  for (const varName in vars) {
    if (vars[varName] === undefined) {
      undefinedVars.push(varName); 
    }
  }

  if (undefinedVars.length)
    throw new Error('These variables are not defined: ' + undefinedVars.join(', '));
}

function preserve (rulesObj) {
  for (const ruleChar in rulesObj) {
    rulesObj[ruleChar] = [ruleChar, ...rulesObj[ruleChar]];
  }

  return rulesObj;
}

function Var(varName) {
  return `__var_${varName}`;
}

function ReadVar(varName) {
  return function(...possibleValues) {
    return { varName, possibleValues }
  }
}

function VarState(stateName, varName) {
  return `__varstate_&&${stateName}&&${varName}`;
}

const $ = new Proxy([], {
  get(target, key) {
    if (key === Symbol.iterator)
      return target[Symbol.iterator].bind(target);
    else {
      target.push(key);
      return $;
    }
  }
});

const megaAlphabet = {
  hebrew: {
    aleph: 'ℵ'
  }
};

const _ = 'Δ';
const [ L, R, W ] = [ 'L', 'R', 'W' ];
const Q0_STATE_DATA = { fallback: [ '-', '-', '-' ] };
const ANY_CHAR = 'any_char';

// fuck alerts (sorry)
alert = (...args) => console.log(...args);

const latexCharReplacement = {
  '▼': '\\blacktriangledown',
  '█': '\\blacksquare'
}

function Environment() {
  return {
    tapes: {},
    machines: {}, /* nanomachines, son */

    Tape(name, { initialState = '', allowedChars = [ ANY_CHAR ] } = {}) {
      this.tapes[name] = { initialState, allowedChars };
      return this;
    },

    Machine(name, { alphabet, program, tapes = ['default_tape'], fallbackInitialPosition = 0, customInitialPositions = {} }) {
      this.machines[name] = { alphabet, program, tapes, fallbackInitialPosition, customInitialPositions };
      return this;
    },

    DefaultMachine({ ...params }) {
      return this.Machine('default_machine', params);
    },

    DefaultTape(initialState) {
      return this.Tape('default_tape', initialState);  
    }
  };
}

function objectOnlyHasProperies(object, propertiesArray) {
  return Object.keys(propertiesArray)
}

/* Use to create rules */
function On({ ...conditions }) {
  const ruleData = {
    conditions,
    tapeActions: {},
    newState: null,
    
    doTapeActions,
    setState
  }

  function doTapeActions({ ...tapeActions }) {
    ruleData.tapeActions = tapeActions;

    delete ruleData.doTapeActions;
    return Object.assign(ruleData, { setState })
  }

  function setState(state) {
    ruleData.newState = state;
    
    delete ruleData.setState;
    return ruleData;
  }

  return Object.assign(ruleData, { setState, doTapeActions });
}

function Not(char) {
  return `__not_${char}`;
}

function Wait(dataToPrint = '', printTargetType=null) {
  return { moveDirection: W, dataToPrint, printTargetType };
}

function GoRight(dataToPrint = '', printTargetType=null) {
  return { moveDirection: R, dataToPrint, printTargetType };
}

function GoLeft(dataToPrint = '', printTargetType=null) {
  return { moveDirection: L, dataToPrint, printTargetType };
}

function Print(charOrVarName, printTargetType='char') {
  return {
    printTargetType,
    dataToPrint: charOrVarName,

    moveDirection: '',
    
    GoRight: GoRight.bind(null, charOrVarName, printTargetType),
    GoLeft: GoLeft.bind(null, charOrVarName, printTargetType),
    Wait: Wait.bind(null, charOrVarName, printTargetType)
  };
}

function PrintVar(varName) {
  return Print(varName, 'var');
}

// TODO GENERATE Q0 FOR EACH PROGRAM
function processState(programToExecute, latexCommands, rules, stateName, varName) {
  const isVarState = !!varName;

  let latexStateName = stateName;
  
  /* q0, q1, ... */
  if (/^q\d+$/.test(stateName)) {
    latexStateName = stateName.replace('q', 'q_{') + '}';
  }

  if (isVarState) {
    latexStateName += `^{${varName}}`;
  }

  for (const ruleChar in stateData) {
    let [ newChar, directionChar, newState ] = stateData[ruleChar];

    let latexRuleChar = ruleChar.startsWith('__var_')
      ? ruleChar.replace('__var_', '')
      : ruleChar;
    
    let latexNewChar = newChar.startsWith('__var_')
      ? newChar.replace('__var_', '')
      : newChar;

    const latexDirectionChar = directionChar;

    let latexNewState;
    if (newState.startsWith('__varstate_')) {
      let [ shortNewStateName, newStateVar ] = newState.split('&&').slice(1);
      
      if (/^q\d+$/.test(shortNewStateName)) {
        shortNewStateName = shortNewStateName.replace('q', 'q_{') + '}';
      }

      latexNewState = shortNewStateName + '^{' + newStateVar + '}';
    } else {
      if (/^q\d+$/.test(newState)) {
        newState = newState.replace('q', 'q_{') + '}';
      }

      latexNewState = newState;
    }

    if (latexRuleChar === '_') {
      latexRuleChar = '\\vd';
    }

    if (latexRuleChar in latexCharReplacement) {
      latexRuleChar = latexCharReplacement[latexRuleChar];
    }

    if (latexNewChar === '_') {
      latexNewChar = '\\vd';
    }

    if (latexNewChar in latexCharReplacement) {
      latexNewChar = latexCharReplacement[latexNewChar];
    }

    if (ruleChar === 'fallback') {
      if (latexStateName === 'q_{0}') {
        continue;
      }

      for (let _ruleChar of []) {
        if (_ruleChar in stateData) {
          continue;
        }

        if (_ruleChar in latexCharReplacement) {
          _ruleChar = latexCharReplacement[_ruleChar];
        }

        latexCommands.push(
          '\\trx'
          + ` {${latexStateName}}`
          + ` {${_ruleChar}}`
          + ` {${_ruleChar}}`
          + ` {${latexNewState}}`
          + ` {${latexDirectionChar}}`
        );
      }

      continue;
    }
    
    latexCommands.push(
      '\\trx'
      + ` {${latexStateName}}`
      + ` {${latexRuleChar}}`
      + ` {${latexNewChar}}`
      + ` {${latexNewState}}`
      + ` {${latexDirectionChar}}`
    );
  }

  Object.assign(programToExecute, { [ isVarState ? VarState(stateName, varName) : stateName ]: stateData });

  return _this;
}

function Program() {
  let _this = new Proxy({
    programData: []
  }, {
    get(target, name) {
      if (name in target)
        return target[name];

      if (name === '_' /* comment */) {
        return function(text) {
          target.programData.push({ type: 'comment', text });
          return _this;
        };
      } else if (name === 'DeclareVar') {
        return function(varName, varPossibleValues) {
          target.programData.push({ type: 'var_declaration', varName, varPossibleValues });
          return _this;
        }
      } else if (name === 'VarState') { /* .VarState('q3')('x')(0,1)([ ...rules ]) */
        return function(stateName) {
          return varName => (...varPossibleValues) =>
            (...rules) => {
              target.programData.push({ type: 'state', isVarState: true, stateName, varName, varPossibleValues, rules });
              return _this;
            }
        };
      } else {
        return function(...rules) {
          target.programData.push({ type: 'state',  isVarState: false, stateName: name, rules });
          return _this;
        };
      }
    }
  });

  return _this;
}