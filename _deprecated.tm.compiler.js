function preprocessProgram(program) {
  for (const stateKey in program) {
    /* If state name is generic, everything inside is paste mode */
    if (stateKey.startsWith('__varstate_')) {
      /* DONE *//* Currently only one var per state */
      /* DONE */const [ stateName, varName ] = stateKey.split('&&').slice(1);
      /* DONE */
      /* DONE */if (!(varName in vars)) {
      /* DONE */  console.error('Переменная должна быть внесена в объект "vars" в файле программы.');
      /* DONE */  console.error({ varName })
      /* DONE */  throw new Error();
      /* DONE */}
      /* DONE */
      /* DONE */for (const possibleValue of vars[varName]) {
      /* DONE */  const currentStateInstance = JSON.parse(JSON.stringify(program[stateKey])) // copy :-)
      /* DONE */
      /* DONE */  for (const ruleKey in currentStateInstance) {
      /* DONE */    if (currentStateInstance[ruleKey][0] === `__var_${varName}`)
      /* DONE */      currentStateInstance[ruleKey][0] = possibleValue;
      /* DONE */
      /* DONE */    if (currentStateInstance[ruleKey][2].startsWith('__varstate_')) {
      /* DONE */      const [ stateName, varName ] = currentStateInstance[ruleKey][2].split('&&').slice(1);
      /* DONE */      currentStateInstance[ruleKey][2] = `__state_&&${stateName}&&${possibleValue}`;
      /* DONE */    }
      /* DONE */
      /* DONE */    if (ruleKey === `__var_${varName}`) {
      /* DONE */      currentStateInstance[possibleValue.toString()] = currentStateInstance[ruleKey];
      /* DONE */      delete currentStateInstance[ruleKey];
      /* DONE */    }
      /* DONE */  }
      /* DONE */  
      /* DONE */  program[`__state_&&${stateName}&&${possibleValue}`] = currentStateInstance;
      /* DONE */}
      /* DONE */
      /* DONE */delete program[stateKey];
    }

    /* Else, possible values are mapped to state rules */
    else {
      const currentStateInstance = program[stateKey];

      for (const ruleKey in currentStateInstance) {
        if (ruleKey.startsWith('__var_')) {
          const varName = ruleKey.replace('__var_', '');
          
          for (const possibleValue of vars[varName]) {
            const ruleCopy = JSON.parse(JSON.stringify(currentStateInstance[ruleKey]));

            if (ruleCopy[0] === `__var_${varName}`)
              ruleCopy[0] = possibleValue;

            if (ruleCopy[2].startsWith('__varstate_')) {
              const [ stateName, varName ] = ruleCopy[2].split('&&').slice(1);
              ruleCopy[2] = `__state_&&${stateName}&&${possibleValue}`;
            }

            currentStateInstance[possibleValue] = ruleCopy;
          }

          delete currentStateInstance[ruleKey];
        }
      }
    }
  }

  return program;
}

program = preprocessProgram(program);