function difference(setA, setB) {
  const result = new Set(setA);
  for (const item of new Set(setB)) {
    result.delete(item);
  }
  return result;
}

const
  runtime = {
    tapes: {},
    machines: {}
  },

  compiledLatex = {
    _chunks: [],

    toSeparatedChunksString: function () {
      return ''; // TODO
    } 
  };

assertNotUndefined({ env, delay, vars });

(() => {
  const { tapes, machines } = env;

  console.log(tapes);
  console.log(machines);

  /* Tapes */
  for (const tapeName in tapes) {
    runtime.tapes[tapeName] = {
      charsWhitelist: [],
      tapeRootElement: null,
      initialState: tapes[tapeName].initialState,
      currentState: tapes[tapeName].initialState,
    }
    
    const { allowedChars } = tapes[tapeName];
    if (!allowedChars.includes('any_char')) {
      runtime.tapes[tapeName].charsWhitelist = allowedChars;
    }
  }

  /* Machines */
  for (const machineName in machines) {
    /* Init */
    const currentMachine = machines[machineName];
    
    assertNotUndefined({
      [`[Alphabet of machine: "${machineName}"]`]: currentMachine.alphabet,
    })

    /* Used later, to fill "vars" */
    const mentionedTapeAnyCharVars = new Set();

    runtime.machines[machineName] = {
      globalVars: {},
      currentState: 'q1', // TODO: think through
      alphabet: currentMachine.alphabet,//TODO: \vd
      allowedTapes: currentMachine.tapes,
      tapePointers: {},
      rules: [],//TODO latex chunks
      statesSet: new Set()
    };

    const currentMachineRuntime = runtime.machines[machineName];

    /* Machine's bound tapes */
    /* In runtime, positions will be replaced with refs to HTML elements */
    for (const tapeName of currentMachine.tapes) {
      currentMachineRuntime.tapePointers[tapeName] = (tapeName in currentMachine.customInitialPositions)
        ? currentMachine.customInitialPositions[tapeName]
        : currentMachine.fallbackInitialPosition;
    }

    /* Machine's program */
    const { programData } = currentMachine.program;

    //'a_1' = any char from tape 1
    //'a_2' = any char from tape 2
    //...
    let currentChunk = []; // TODO
    
    for (const programPart of programData) {
      /* Currently, only makes sense in latex representation */
      if (programPart.type === 'comment') {
        const { text } = programPart;
        
        currentChunk.push(`\\tpp{${text}}`);
        continue;
      }

      /* State preprocessing */
      if (programPart.type === 'state') {
        const { isVarState, stateName, rules } = programPart;

        const stateLevelVariables = {};

        if (isVarState) {
          const { varName, varPossibleValues } = programPart;

          if (varName in stateLevelVariables)
            throw new Error(`State-level variable ${varName} at state ${stateName} already defined earlier.`)

          stateLevelVariables[varName] = varPossibleValues;
        }

        currentMachineRuntime.statesSet.add(stateName);

        /*
          Get rid of ANY_CHAR from tapes allowed chars lists
          also, get rid of duplicates in alphabets (why not)
        */
        for (const tapeName in tapes) {
          tapes[tapeName].allowedChars =
            tapes[tapeName].allowedChars.includes(ANY_CHAR)
              ? currentMachine.alphabet
              : Array.from(new Set(tapes[tapeName].allowedChars));
          
          runtime.tapes[tapeName].allowedChars = tapes[tapeName].allowedChars;
        }

        /* Compiling "metarules" */
        for (const rule of rules) {
          const compiledRule = {
            localVariables: {},
            conditionsTypes: {},
            conditions: {},
            actions: {},
            nextState: null
          };

          const addLocalVariable = (varName, possibleValues) => {
            if (varName in compiledRule.localVariables) {
              throw new Error(`Local variable "${varName} already exists.`)
            }

            if (varName in stateLevelVariables) {
              throw new Error(`Error defining rule-local variable "${varName}": a state-local variable with this name already exists.`
                + `Same names are not allowed for unambiguity.`)
            }

            compiledRule.localVariables[varName] = possibleValues;
          }

          const { conditions, tapeActions, newState } = rule;
          
          /* Conditions */
          for (const tapeName in conditions) {
            const currentTapeId = currentMachine.tapes.indexOf(tapeName) + 1;

            const specifiedValue = conditions[tapeName];

            /* Not string => var */
            if (specifiedValue.hasOwnProperty('varName')) {
              const { varName, possibleValues } = specifiedValue;
              
              addLocalVariable(varName, possibleValues);
              compiledRule.conditions[tapeName] = varName;
              compiledRule.conditionsTypes[tapeName] = 'var';
            }

            /* Not(char) -> all tape chars except char */
            else if (specifiedValue.toString().startsWith('__not_')) {
              const char = specifiedValue.replace('__not_', '');
              const possibleValues = Array.from(difference(
                new Set(tapes[tapeName].allowedChars),
                new Set([ char ])));

              const localVarName = `c_${currentTapeId}`;

              addLocalVariable(localVarName, possibleValues);
              compiledRule.conditions[tapeName] = localVarName;
              compiledRule.conditionsTypes[tapeName] = 'var';
            }
            
            /* 'any_char' */
            else if (specifiedValue === ANY_CHAR) {
              const globalVarName = `a_${currentTapeId}`;
              
              mentionedTapeAnyCharVars.add(globalVarName);
              compiledRule.conditions[tapeName] = globalVarName;
              compiledRule.conditionsTypes[tapeName] = 'var';
            }
            
            /* just some char */
            else {
              compiledRule.conditions[tapeName] = specifiedValue;
            }
          }

          const tapesNotMentionedInConditions = difference(
            currentMachine.tapes,
            Object.keys(conditions)
          )

          for (const tapeName of tapesNotMentionedInConditions) {
            const currentTapeId = currentMachine.tapes.indexOf(tapeName) + 1;
            const globalVarName = `a_${currentTapeId}`;

            mentionedTapeAnyCharVars.add(globalVarName);
            compiledRule.conditions[tapeName] = globalVarName;
            compiledRule.conditionsTypes[tapeName] = 'var';
          }

          /* Actions */
          for (const tapeName in tapeActions) {
            const { moveDirection, dataToPrint, printTargetType } = tapeActions[tapeName];

            compiledRule.actions[tapeName] = {
              moveDirection: (moveDirection === '')
                ? compiledRule.conditions[tapeName]
                : moveDirection,
              
              dataToPrint: (dataToPrint === '')
                ? compiledRule.conditions[tapeName]
                : dataToPrint,

              printTargetType: printTargetType || ((compiledRule.conditionsTypes[tapeName] === 'var')
                ? 'var'
                : 'char') /* Because as of now conditionsTypes[tapeName] is either 'var' or undefined. */
            }
          }

          const tapesNotMentionedInActions = difference(
            currentMachine.tapes,
            Object.keys(tapeActions)
          );

          for (const tapeName of tapesNotMentionedInActions) {
            const printTarget = compiledRule.conditions[tapeName];

            compiledRule.actions[tapeName] = {
              printTargetType: (mentionedTapeAnyCharVars.has(printTarget)) || [stateLevelVariables, compiledRule.localVariables]
                .some(variableStore => (printTarget in variableStore))
                  ? 'var'
                  : 'char',
              dataToPrint: compiledRule.conditions[tapeName],
              moveDirection: 'W'
            }
          }

          compiledRule.stateLevelVariables = stateLevelVariables;

          /* Current and next state */

          compiledRule.state = stateName;

          compiledRule.isStateVarState = isVarState;

          compiledRule.nextState =
            (newState === null)
              ? stateName
              : newState;

          console.log('CR', { compiledRule });

          currentMachineRuntime.rules.push(compiledRule);
        }
      }
    }

    for (const varName of mentionedTapeAnyCharVars) {
      const [, ix] = varName.split('_');
      const tapeName = currentMachine.tapes[parseInt(ix) - 1];
      
      currentMachineRuntime.globalVars[varName] = tapes[tapeName].allowedChars;
    }

    currentMachineRuntime.statesSet.add('q0');
  }
})();
