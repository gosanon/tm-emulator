/* UI utils */

function setProperties (element, props) {
  Object.keys(props).map(prop => {
    element[prop] = props[prop];
  });
  return element;
}

function setStyle (element, style) {
  Object.keys(style).map(name => {
    element.style[name] = style[name];
  });
  return element;
}

function ExtendedElement(el) {
  el.prependTo = parent => { parent.prepend(el); return el; };
  el.appendTo = parent => parent.appendChild(el);
  el.extend  = async (innerSelector, buildFunction) => el.querySelector(innerSelector).appendChild(await buildFunction());
	el.setProperties = function (arg) { return setProperties(this, arg); };
	el.setStyle = function (arg) { return setStyle(this, arg); };
  el.displayFlexContentCentered = () => el.setStyle({
    display: 'flex', justifyContent: 'center', alignItems: 'center'
  });
  el.displayFlexContentCenteredHorizontally = () => el.setStyle({
    display: 'flex', justifyContent: 'center'
  });
  el.fillParent = () => el.setStyle({ width: '100%', height: '100%', top: 0, left: 0 });

  return el;
}

function createElement (tag, children = []) {
  let el = document.createElement(tag);

  if (children)
    children.forEach(item => el.appendChild(item));
  
  return ExtendedElement(el);
}

function CodeCellElement(text) {
  return createElement('span', [
      createElement('a')
        .setProperties({ textContent: text, className: 'code-cell-text' }) ])
    .setProperties({ className: 'code-cell' });
}

/* Runtime init (including UI) */

const { tapes, machines } = runtime; /* Not "env" */

for (const tapeName in tapes) {
  const localizedTapeName = tapeName => {
    const tapeNameLocalization = {
      source_tape: 'Исходная лента',
      buffer_tape: 'Вспомогательная лента'
    };
    
    return (tapeName in tapeNameLocalization)
      ? tapeNameLocalization[tapeName]
      : tapeName;
  }

  const tape = tapes[tapeName];

  const tapeRootElement = createElement('div', [
    createElement('div')
      .setProperties({
        className: 'tape-name',
        textContent: localizedTapeName(tapeName)
      }),

    createElement('div').setProperties({
      className: 'tape-allowed-chars',
      textContent: `Допустимые символы: ${ tape.charsWhitelist.length !== 0 ? ('"' + ['Δ', tape.charsWhitelist].join('", "') + '"') : 'любые' }`
    }),

    createElement('div').setProperties({ className: 'tape-initial-state' }),
  ])
    .setProperties({ className: 'tape-root', id: `tape-${tapeName}` })
    .appendTo(document.querySelector('#tapes'));

  [
    createElement('span').setProperties({ textContent: 'Начальное состояние:' }),
    (tape.initialState !== '')
      ? CodeCellElement(tape.initialState)
      : createElement('a').setProperties({ textContent: 'пусто', className: 'just-empty' })
  ].forEach(el => el.appendTo(tapeRootElement.querySelector('.tape-initial-state')));

  createElement('div', [
    /* Tape cells */
    ...(
      /* No edges because we don't really need them? */
      [ /* '', */ ...(tape.initialState.length !== 0 ? tape.initialState : ['']), /* '' */ ]
        .map(char => createElement('span')
        .setProperties({ textContent: char, className: 'tape-cell' }))),
  ])
    .setProperties({ className: 'tape' })
    .appendTo(tapeRootElement);
  
  tapeRootElement.querySelectorAll('.tape-cell')[0].classList.add('tm-pointer-here');

  // name, allowed characters
  // state (visual)

  Object.assign(tapes[tapeName], { tapeRootElement });
}

for (const machineName in machines) {
  const machine = machines[machineName];
  
  /* Replase numeric positions with HTML elements */
  for (const tapeName in machine.tapePointers) {
    machine.tapePointers[tapeName] = tapes[tapeName]
      .tapeRootElement
      .querySelector('.tape')
      .children[machine.tapePointers[tapeName]];
  }

  const localizedMachineName = machineName => {
    const machineNameLocalization = { default_machine: 'Основная машина' };
    
    return (machineName in machineNameLocalization)
      ? machineNameLocalization[machineName]
      : machineName;
  }
  
  let
    machineNameElement,
    machineAlphabetElement;

  const machineRootElement = createElement('div', [
    createElement('div')
      .setProperties({
        className: 'machine-name',
        textContent: localizedMachineName(machineName)
      }),

    createElement('div')
      .setProperties({
        className: 'machine-alphabet',
        textContent: `Распознаёт символы: "${ [...machine.alphabet].join('", "') }"`
      }),

    createElement('div')
      .setProperties({
        className: 'machine-states'
      })

    // TODO display next command, maybe more current state data
  ])
  .setProperties({ className: 'machine-root' })
  .appendTo(document.querySelector('#machines'));
  
  [
    createElement('a').setProperties({ className: 'machine-states-header', textContent: 'Состояние:' }),
        ...[...machine.statesSet]
          .sort()
          .map(stateName =>
            createElement('span')
              .setProperties({
                className: 'state-cell',
                id: `machine-${machineName}-state-${stateName}`,
                textContent: stateName }))
  ].forEach(el => machineRootElement.querySelector('.machine-states').appendChild(el));

  machineRootElement.querySelector('.machine-states').children[2].classList.add('tm-pointer-here');

  Object.assign(machines[machineName], { machineRootElement });
  // name, linked tapes
  // current state data (?)
}

console.log({ tapes, machines });

/* Actual machine runtime */

function getCurrentTapesData(currentCellByTapeName) {
  const chars = {}

  for (const tapeName in currentCellByTapeName) {
    /* Assume left empty char always exists */
    const element = currentCellByTapeName[tapeName];
    const value = (element.textContent === '')
      ? 'Δ'
      : element.textContent;
    
    chars[tapeName] = { element, value };
  }

  return chars;
}

function verifyConditions(currentState, tapeData, state, conditions, localVariables, stateLevelVariables, machineLevelVariables) {
  const variables = {};
  const success = (currentState === state)
    && [...Object.keys(tapeData)].every(tapeName => {
      const mapStr = arr => arr.map(el => el.toString());

      const value = tapeData[tapeName].value.toString();
      const target = conditions[tapeName].toString();

      /* Possibly a ReadVar */
      const canBeLocalVar = target in localVariables;
      const canBeStateLevelVar = target in stateLevelVariables;
      const canBeMachineLevelVar = target in machineLevelVariables;
      const canBeAChar = mapStr(tapes[tapeName].allowedChars).includes(value);

      return (canBeLocalVar
          && mapStr(localVariables[target]).includes(value)
          && (variables[target] = value))
        || (canBeStateLevelVar
          && mapStr(stateLevelVariables[target]).includes(value)
          && (variables[target] = value))
        || (canBeMachineLevelVar
          && mapStr(machineLevelVariables[target]).includes(value)
          && (variables[target] = value))
        || canBeAChar
          && (value.toString() === target.toString());      
    });
  
  console.log({ currentState, tapeData, state, conditions, localVariables, stateLevelVariables });

  return { success, variables };

}

// TODO move left, move right -> create or delete empty cells on edges

let runtimeInterval;

function tick() {
  /* Multiple machines support possibly later */
  const currentMachine = machines.default_machine;

  const tapeData = getCurrentTapesData(currentMachine.tapePointers);

  const currentState = currentMachine.currentState;

  console.log({ tapeData, currentState });
  
  let foundCorrespondingRule = false;
  for (const { state, conditions, localVariables, stateLevelVariables, actions, nextState } of currentMachine.rules) {
    const machineLevelVariables = currentMachine.globalVars;

    const { success, variables } = verifyConditions(currentState, tapeData, state, conditions, localVariables, stateLevelVariables, machineLevelVariables);

    if (success) {
      for (const tapeName in actions) {
        const { printTargetType, dataToPrint, moveDirection } = actions[tapeName];

        /* Printing */
        // TODO add validation
        let valueToPrint = printTargetType === 'var' ? variables[dataToPrint] : dataToPrint;
        if (valueToPrint === _) {
          valueToPrint = '';
        }

        const tapeElement = tapes[tapeName].tapeRootElement.querySelector('.tape');

        const currentCellElement = currentMachine.tapePointers[tapeName];
        const currentPosition = [...currentCellElement.parentNode.children].indexOf(currentCellElement);

        /* Setting new value, moving pointer */

        let pointedElement;
        if (pointedElement = tapeElement.querySelector('.tm-pointer-here')) {
          pointedElement.classList.remove('tm-pointer-here');
        }

        tapeElement.children[currentPosition].textContent = valueToPrint;

        const childrenCount = tapeElement.children.length;

        console.log({ moveDirection, valueToPrint });

        /* Creating/removing empty cells */
        if (moveDirection === R) {
          let currentPointerSet;
          if (currentPosition === childrenCount - 1) {
            createElement('span')
              .setProperties({ className: 'tape-cell', textContent: '' })
              .appendTo(tapeElement)
              
            currentMachine.tapePointers[tapeName] = currentCellElement.nextSibling;
            currentPointerSet = true;
          }

          if ((currentPosition === 0) && (valueToPrint === '')) {  
            currentMachine.tapePointers[tapeName] = currentCellElement.nextSibling;
            currentPointerSet = true;
            tapeElement.firstElementChild.remove();
          }

          if (!currentPointerSet) {
            currentMachine.tapePointers[tapeName] = currentCellElement.nextSibling;
          }
        }

        else if (moveDirection === L) {
          let currentPointerSet;
          // console.log({ currentPosition, childrenCount, dataToPrint });
          // throw new Error();
          if (currentPosition === 0) {
            createElement('span')
              .setProperties({ className: 'tape-cell', textContent: '' })
              .prependTo(tapeElement)
            currentMachine.tapePointers[tapeName] = currentCellElement.previousSibling;
            currentPointerSet = true;
          }

          if ((currentPosition === childrenCount - 1) && (valueToPrint === '')) {
            currentMachine.tapePointers[tapeName] = currentCellElement.previousSibling;
            currentPointerSet = true;
            tapeElement.lastElementChild.remove();
          }

          if (!currentPointerSet) {
            currentMachine.tapePointers[tapeName] = currentCellElement.previousSibling;
          }
        }

        currentMachine.tapePointers[tapeName].classList.add('tm-pointer-here');
      }

      /* Update current state and its marker */
      if (currentMachine.currentState !== nextState) {
        currentMachine.currentState = nextState;
        currentMachine.machineRootElement.querySelector('.tm-pointer-here').classList.remove('tm-pointer-here');
        currentMachine.machineRootElement.querySelector(`#machine-default_machine-state-${currentMachine.currentState}`).classList.add('tm-pointer-here');
      }

      if (currentMachine.currentState === 'q0') {
        clearInterval(runtimeInterval);
      }
    }
  }


  // const {
  //   tapesNewChars,
  //   tapesNewDirections,
  //   newMachineState } = machines.default_machine.processInput(currentCharsByTape);
}

runtimeInterval = setInterval(tick, delay);

/* ********************************************************************* */

// if (program === undefined) {
//   console.error('В файле программы не задана переменная "program".');
//   throw new Error();
// }

// if (tapeState === undefined) {
//   console.error('В файле программы не задана переменная "tapeState".');
//   throw new Error();
// }

// console.log(program.latex());

// program = program.build();

// console.log(program);

// console.log(program);

// empty = '_'
// const empty_space_around_limit = 10 // todo get rid of it

// function replaceAt(string, index, replace) {
//   return string.substring(0, index) + replace + string.substring(index + 1);
// }

// startFrom += empty_space_around_limit

// ///

// /// generate empty field
// for (i = 0; i < empty_space_around_limit; i++) {
//   tapeState = empty + tapeState
//   tapeState += empty
// }

// for (i = 0; i < tapeState.length; i++){
//   cur = document.createElement('p')
//   cur.style.display = 'inline-block'
//   cur.innerHTML = tapeState[i]
//   cur.setAttribute('id', 'let'+i)
//   document.getElementById('main').appendChild(cur)
// }

// sts = document.createElement('p')
// sts.style.display = 'inline-block'
// sts.setAttribute('id', 'status')
// document.getElementById('main').appendChild(sts)

// /* code of the machine */

// let
//   k = 0,
//   currentState = 'q1',
//   currentIndex = startFrom;

// const machine = setInterval(function() {
//   if (currentState == 'q0') {
//     clearInterval(machine);
//     sts.innerHTML = '&nbsp&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp&nbsp'
//       + currentState
//       + '&nbsp&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp&nbspcommands: '
//       + '-';
//     return;
//   }

//   sts.innerHTML = '&nbsp&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp&nbsp'
//     + currentState
//     + '&nbsp&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp&nbspcommands: '
//     + program[currentState][tapeState[currentIndex]];

//   if (!(currentState in program)) {
//     console.error('Информации о текущем состоянии нет в программе? Wait, what?');
//     console.error({ currentState, program });
//     clearInterval(machine);
//     return alert('Ошибка ахах лол');
//   }

//   const newMachineStateData = program[currentState];

//   let newChar, newPositionDirection, newState;

//   if (tapeState[currentIndex] in newMachineStateData) {
//     const dataByChar = newMachineStateData[tapeState[currentIndex]];
    
//     if (dataByChar.length !== 3) {
//       console.error('Мало параметров в правиле (а надо 3).');
//       console.error(dataByChar);
//       clearInterval(machine);
//       return alert('Ошибка ахах лол');
//     }

//     [ newChar, newPositionDirection, newState ] = dataByChar;
//   } else if ('fallback' in newMachineStateData) {
//     const dataByChar = newMachineStateData.fallback;
    
//     if (dataByChar.length !== 3) {
//       console.error('Мало параметров в правиле (а надо 3).');
//       console.error(dataByChar);
//       clearInterval(machine);
//       return alert('Ошибка ахах лол');
//     }

//     [ newChar, newPositionDirection, newState ] = dataByChar;

//     if (newChar == 'fallback' /* "preserve" */)
//       newChar = tapeState[currentIndex];
//   } else {
//     console.error('Не найдено правило для обработки символа. Fallback также отсутствует.');
//     console.error({ stateData: tapeState[currentIndex], newMachineStateData });
//     clearInterval(machine);
//     return alert('Ошибка ахах лол');
//   }

//   if (!(newPositionDirection in { R, L, W, '-': '-' })) {
//     console.error('Странный указатель направления))');
//     console.error({ newPositionDirection });
//     clearInterval(machine);
//     return alert('Ошибка ахах лол');
//   }

//   tapeState = replaceAt(tapeState, currentIndex, newChar);
//   document.getElementById('let' + currentIndex).innerHTML = newChar;

//   currentIndex += ({ R: +1, L: -1, W: 0 }[newPositionDirection]);

//   currentState = newState;

//   k++;

//   for (i = 0; i < tapeState.length; i++) {
//     document.getElementById('let' + i).style.backgroundColor = '#FFF';
//     if (i == currentIndex) {
//       document.getElementById('let' + i).style.backgroundColor = '#0F0';
//     }
//   }

//   console.log(currentState);

//   console.log(
//     k
//     + ' | '
//     + tapeState
//     + ' | '
//     + currentState
//     +' | commands: '
//     + program[currentState][newChar]
//     + ' '
//     + currentIndex);
// }, delay)