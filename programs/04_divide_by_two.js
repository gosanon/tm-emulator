delay = 700;

// TODO q00
env = Environment()
  .Tape('source_tape', { initialState: '0110' })
  .Tape('buffer_tape')

  .DefaultMachine({
    tapes: [ 'source_tape', 'buffer_tape' ],
    alphabet: [ _, 0, 1 ],
    program: Program()
      ._('Поскольку ввод начинается с "0", копируем его.')
      .q1(
        On({ source_tape: 0 })
        .doTapeActions({
          source_tape: Print(_).GoRight(),
          buffer_tape: Print(0).GoRight() })
        .setState('q2')
      )

      ._('Копируем: нечётные индексы. Встретив 0, завершаем работу в состояние q0 (деление завершено).')
      .q2(
        On({ source_tape: 1 })
        .doTapeActions({ source_tape: Print(_).GoRight() })
        .setState('q3'),

        On({ source_tape: 0 })
        .doTapeActions({
          source_tape: Print(_).GoRight(),
          buffer_tape: Print(0) })
        .setState('q4')
      )

      ._('Копируем: чётные индексы. Встретив 0, понимаем, что исходное число - нечётное.')
      ._('Поэтому в таком случае переходим в q00 и больше ничего не делаем.')
      .q3(
        On({ source_tape: 1 })
        .doTapeActions({
          source_tape: Print(_).GoRight(),
          buffer_tape: Print(1).GoRight() })
        .setState('q2'),

        On({ source_tape: 0 })
        .setState('q00')
      )

      ._('Копируем справа налево всё обратно на первую ленту. А после - конец.')
      .q4(
        On({ buffer_tape: ReadVar('x')(0, 1) })
        .doTapeActions({
          source_tape: PrintVar('x').GoLeft(),
          buffer_tape: Print(_).GoLeft() }),

        On({ buffer_tape: _ })
        .setState('q0')
      )
  });