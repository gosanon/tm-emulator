// TODO STATE RENAMING

compiledSubprograms = {
  /*
    Starting position: first 1 in 11..1 sequence.
    Ending position: end of updated 11..1 sequence.
    Multiplies sequence by <num>.
  */
  addConst(num, targetTape='default_tape') {
    let program = Program()
      ._('Пропускаем единицы')
      .q1(
        On({ [targetTape]: 1 })
        .doTapeActions({ [targetTape]: GoRight() }),

        On({ [targetTape]: _ })
        .setState('q2')
      )

      ._('Печатаем *num* единиц, конец.');

    for (let i = 1; i < num; i++) {
      program = program[`q${i + 1}`](
        On({ [targetTape]: ANY_CHAR })
        .doTapeActions({ [targetTape]: Print(1).GoRight() })
      )
    }

    program = program[`q${num + 1}`](
      On({ [targetTape]: ANY_CHAR })
      .doTapeActions({ [targetTape]: Print(1).GoRight() })
      .setState('q0')
    );

    return { program };
  },

  /*
    Starting position: first 1 in 11..1 sequence.
    Multiplies sequence by <num> (>= 1).
  */
  mulConst(num, targetTape='default_tape',
      leftBorderChar=megaAlphabet.hebrew.aleph,
      rightBorderChar=megaAlphabet.hebrew.aleph,
      pointerChar='') {
    
    /* Init */
    let program = Program();

    /* Subprogram params */
    requiredAdditionalChars = new Set([ leftBorderChar, rightBorderChar ]);

    /* Subprogram code */

    const bordersCharsMatch = leftBorderChar === rightBorderChar;
    const anyBorderCharMatchesPointer = (leftBorderChar === pointerChar)
      || (rightBorderChar === pointerChar);

    if (anyBorderCharMatchesPointer) {
      throw new Error('anyBorderCharMatchesPointer');
    }

    program = program
      ._('Ставим левую границу вместо первой 1')
      .q1(
        On({ [targetTape]: 1 })
        .doTapeActions({ [targetTape]: Print(leftBorderChar).GoRight() })
        .setState('q2')
      )

      ._('Сразу встретили не 1 => число равно 1')
      .q2(
        On({ [targetTape]: Not(1) })
        .doTapeActions({ [targetTape]: GoLeft() })
        .setState('q3'),

        On({ [targetTape]: 1 })
        .setState('q4')
      )

      ._('Число равно 1. Убираем левую границу и прибавляем <num - 1>.')
      .q3(
        On({ [targetTape]: leftBorderChar })
        .initSubprogram('addConst', num - 1)
        .then({ success: setState('q0') /* , failure: setState('q00') */ })
      )

      ._('Число больше 1. Ищем правую границу.')
      .q4(
        On({ [targetTape]: 1 })
        .doTapeActions({ [targetTape]: GoRight() }),

        On({ [targetTape]: Not(1) })
        .doTapeActions({ [targetTape]: GoLeft() })
        .setState('q5')
      )

      ._('Ставим правую границу.')
      .q5(
        On({ [targetTape]: 1 })
        .doTapeActions({ [targetTape]: Print(rightBorderChar).GoLeft() })
        .setState('q6')
      )
      
      ._('<num> раз копируем последовательность.');

    program = program
      ._('Начинаем на правой границе')
      .q5(
        //todo
        //set '
        //.........
        //todo also think about additional tape; also think about (* 1) 
      )

    return { program, requiredAdditionalChars };
  }
}

// todo use hasOwnProperty(<param name>)