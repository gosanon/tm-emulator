/* Should (re-)assign "program" and "tapeState" variables */

delay = 700;

tapeState = "1010";

vars = {
  x: [0, 1]
};

// TODO add validation: machine alphabet must include all linked tapes alphabets
// TODO __var_x & autopaste x issue
// TODO think through case when type of nextState is VarState
// TODO test Not(_) vs Not(0) vs Not(1)
// TODO? Not(Var) support?
// TODO? not only create var in conditions, but check for it too (e.g. if state-local)?
env = Environment()
  .Tape('source_tape', { initialState: '0111' })
  .Tape('buffer_tape')

  .DefaultMachine({
    tapes: [ 'source_tape', 'buffer_tape' ],
    alphabet: [ _, 0, 1 ],
    program: Program()
      ._('Идём направо до пустого символа, копируя непустые на вторую ленту.')
      .q1(
        On({ source_tape: ReadVar('x')(0, 1) })
        .doTapeActions({
          source_tape: GoRight(),
          buffer_tape: PrintVar('x').GoRight()
        }),

        On({ source_tape: _ })
        .doTapeActions({ buffer_tape: GoLeft() })
        .setState('q2')
      )

      ._('Сдвигаем указатель второй ленты на начало строки.')
      .q2(
        On({ buffer_tape: _ })
        .doTapeActions({ buffer_tape: GoRight() })
        .setState('q3'),

        On({ buffer_tape: Not(_) })
        .doTapeActions({ buffer_tape: GoLeft() })
      )

      ._('Переносим непустые символы со второй ленты на первую, встретили пустой - конец.')
      .q3(
        On({ buffer_tape: ReadVar('x')(0, 1) })
        .doTapeActions({
          source_tape: PrintVar('x').GoRight(),
          buffer_tape: Print(_).GoRight()
        }),

        On({ buffer_tape: _ })
        .setState('q0')
      )
  });

